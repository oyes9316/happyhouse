package com.mycom.myapp.dto;

public class UserDto {
	/** 회원 아이디 */
	private String id;
	/** 회원 비밀번호 */
	private String password;
	/** 회원 이름 */
	private String name;
	/** 회원 주소 */
	private String address;
	/** 회원 전화번호 */
	private String phone;
	/** 관리자 여부 */
	private String isAdmin;
	/** 질문 */
	private String question;
	/** 답 */
	private String answer;

	public UserDto() {
	}

	public UserDto(String id, String password, String name, String address, String phone, String isAdmin,
			String question, String answer) {
		super();
		this.id = id;
		this.password = password;
		this.name = name;
		this.address = address;
		this.phone = phone;
		this.isAdmin = isAdmin;
		this.question = question;
		this.answer = answer;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

}
