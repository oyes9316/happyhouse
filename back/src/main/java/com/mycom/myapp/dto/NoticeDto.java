package com.mycom.myapp.dto;

import java.util.Date;

public class NoticeDto {
   private int notice_Id;
   private String user_Name;
   private String title;
   private String content;
   private Date reg_Dt;
   private int read_Count;

   public int getNoticeId() {
      return notice_Id;
   }

   public void setNoticeId(int noticeId) {
      this.notice_Id = noticeId;
   }

   public String getUserName() {
      return user_Name;
   }

   public void setUserName(String userName) {
      this.user_Name = userName;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public String getContent() {
      return content;
   }

   public void setContent(String content) {
      this.content = content;
   }

   public Date getRegDt() {
      return reg_Dt;
   }

   public void setRegDt(Date regDt) {
      this.reg_Dt = regDt;
   }

   public int getReadCount() {
      return read_Count;
   }

   public void setReadCount(int readCount) {
      this.read_Count = readCount;
   }

}