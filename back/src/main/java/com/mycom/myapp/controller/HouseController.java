package com.mycom.myapp.controller;

import java.util.Iterator;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mycom.myapp.dto.CodeDto;
import com.mycom.myapp.dto.HouseDeal;
import com.mycom.myapp.dto.HouseInfo;
import com.mycom.myapp.service.HouseService;

@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
public class HouseController {

	@Autowired
	HouseService service;
	
	@GetMapping(value="/house/sido")
	public List<CodeDto> sido() {
		System.out.println("sido() 호출");
		return service.sido();
	}

	@GetMapping(value="/house/gugun/{sidoCode}")
	public List<CodeDto> gugun(@PathVariable String sidoCode) {
		System.out.println("gugun() 호출, "+sidoCode);
		return service.gugun(sidoCode);
	}
	
	@GetMapping(value="/house/dong/{gugunCode}")
	public List<CodeDto> dong(@PathVariable String gugunCode) {
		System.out.println("dong() 호출, "+gugunCode);
		return service.dong(gugunCode);
	}

	@GetMapping(value="/house/getGeoList/{dong}")
	public List<HouseInfo> geoList(@PathVariable String dong) {
		System.out.println("getGeoList() 호출, "+dong);
		List<HouseInfo> list = service.getGeoList(dong);
		System.out.println(list.size());
		return list;
	}
	
	@GetMapping(value="/house/getTransactionList/{dong}/{checked}")
	public List<HouseDeal> transactionList(@PathVariable String dong, @PathVariable String[] checked) {
		System.out.print("getTransactionList() 호출, "+dong+", ");
		for(int i=0; i<checked.length; i++) {
			System.out.print(checked[i]+" ");
		}
		System.out.println();
		List<HouseDeal> list = service.getTransactionList(dong, checked);
		return list;
	}
	
	@GetMapping(value="/house/web")
	public String[][] Crawling () throws Exception {
		String url = "https://land.naver.com/news/best.nhn?best_tp_cd=WW";
		System.out.println("==================================");
		Document doc = Jsoup.connect(url).get();
		Elements element = doc.select("ul.rank_list");

		Iterator<Element> e1 = element.select("img").iterator();
		Iterator<Element> e2 = element.select("dt").not(".photo").iterator();
		Iterator<Element> e3 = element.select("dd").iterator();
		Iterator<Element> e4 = element.select(".photo a").iterator();
		String[][] news = new String[5][4];
		for(int i=0; i<5; i++) {
			news[i][0] = e1.next().attr("src");
			news[i][1] = e2.next().text();
			news[i][2] = e3.next().text();
			news[i][3] = e4.next().attr("href");
		}
		
		return news;
	}
}
