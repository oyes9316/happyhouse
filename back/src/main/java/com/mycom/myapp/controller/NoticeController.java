package com.mycom.myapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycom.myapp.dto.NoticeDto;
import com.mycom.myapp.service.NoticeService;

//http://localhost:9999/vue/swagger-ui.html
@CrossOrigin(origins = { "*" }, maxAge = 6000)
@RestController
@RequestMapping("/notice")
public class NoticeController {

	private static final String SUCCESS = "success";
	private static final String FAIL = "fail";

	@Autowired
	private NoticeService noticeService;

	@GetMapping
	public ResponseEntity<List<NoticeDto>> retrieveNotice() throws Exception {
		System.out.println("여기까지옴");
		return new ResponseEntity<List<NoticeDto>>(noticeService.retrieveNotice(), HttpStatus.OK);
	}

	@GetMapping("/{notice_Id}")
	public ResponseEntity<NoticeDto> detailNotice(@PathVariable int notice_Id) {
		System.out.println("detail");
		return new ResponseEntity<NoticeDto>(noticeService.detailNotice(notice_Id), HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<String> writeNotice(@RequestBody NoticeDto notice) {
		if (noticeService.writeNotice(notice)) {
			return new ResponseEntity<String>(SUCCESS, HttpStatus.OK);
		}
		return new ResponseEntity<String>(FAIL, HttpStatus.NO_CONTENT);
	}

	@PutMapping("/{notice_Id}")
	public ResponseEntity<String> updateNotice(@RequestBody NoticeDto notice) {

		if (noticeService.updateNotice(notice)) {
			return new ResponseEntity<String>(SUCCESS, HttpStatus.OK);
		}
		return new ResponseEntity<String>(FAIL, HttpStatus.NO_CONTENT);
	}

	@DeleteMapping("{notice_Id}")
	public ResponseEntity<String> deleteNotice(@PathVariable int notice_Id) {
		if (noticeService.deleteNotice(notice_Id)) {
			return new ResponseEntity<String>(SUCCESS, HttpStatus.OK);
		}
		return new ResponseEntity<String>(FAIL, HttpStatus.NO_CONTENT);
	}
}