package com.mycom.myapp.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mycom.myapp.dto.UserDto;
import com.mycom.myapp.service.UserService;

@CrossOrigin(origins = { "*" }, maxAge = 6000)
@Controller
public class LoginController {

	@Autowired
	UserService userService;

	// For Vue Login Test
	// NO DB
	// Only Check 'ssafy'-'ssafy'
	@PostMapping(value = "/login")
	@ResponseBody
	public ResponseEntity<UserDto> login(@RequestBody UserDto user) throws SQLException {

		UserDto userDto = userService.login(user.getId(), user.getPassword());

		if (userDto != null) {
			return new ResponseEntity<UserDto>(userDto, HttpStatus.OK);
		} else {
			return new ResponseEntity<UserDto>(userDto, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(value = "/join")
	public ResponseEntity<UserDto> join(@RequestBody UserDto user) {
		userService.join(user);
		return new ResponseEntity<UserDto>(user, HttpStatus.OK);
	}

	@PostMapping(value = "/update")
	@ResponseBody
	public ResponseEntity<UserDto> update(@RequestBody UserDto user) {
		UserDto usertmp = user;
		userService.update(user);

		return new ResponseEntity<UserDto>(usertmp, HttpStatus.OK);
	}
	
	@PostMapping(value = "/delete")
	public ResponseEntity<UserDto> delete(@RequestBody UserDto user) {
		UserDto usertmp = user;
		userService.delete(user.getId());

		return new ResponseEntity<UserDto>(usertmp, HttpStatus.OK);
	}

	@RequestMapping(value = "/logout")
	public ResponseEntity<String> logout(HttpSession session) {
		session.invalidate();
		return new ResponseEntity<String>("logout", HttpStatus.OK);
	}

	// For Vue URL Mapping Test
	@RequestMapping(value = "/vuelogin")
	public String vueLogin() {
		return "redirect:/sfc/login/login.html";
	}

	// For Vue URL Mapping Test
	@RequestMapping(value = "/vueboard")
	public String vueBoard() {
		return "redirect:/sfc/board/index.html";
	}
}
