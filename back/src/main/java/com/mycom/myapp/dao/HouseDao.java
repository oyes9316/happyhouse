package com.mycom.myapp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mycom.myapp.dto.CodeDto;
import com.mycom.myapp.dto.HouseDeal;
import com.mycom.myapp.dto.HouseInfo;

@Mapper
public interface HouseDao {

	public List<CodeDto> sido();

	public List<CodeDto> gugun(String sidoCode);

	public List<CodeDto> dong(String gugunCode);

	public List<HouseInfo> getGeoList(@Param("dong") String dong);
	
	public List<HouseDeal> getTransactionList(@Param("dong") String dong, @Param("checked") String[] checked);
}