package com.mycom.myapp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.mycom.myapp.dto.NoticeDto;

@Mapper
public interface NoticeDao {

	public int noticeInsert(NoticeDto dto);

	public int noticeUpdate(NoticeDto dto);

	public int noticeDelete(int NoticeId);

	public NoticeDto noticeDetail(int NoticeId);

	public List<NoticeDto> noticeList();

	public int noticeListTotalCnt();

	public List<NoticeDto> noticeListSearchWord(int limit, int offset, String searchWord);

	public int noticeListSearchWordTotalCnt(String searchWord);
}
