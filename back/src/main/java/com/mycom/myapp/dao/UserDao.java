package com.mycom.myapp.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.mycom.myapp.dto.UserDto;

@Mapper
public interface UserDao {

	public UserDto login(@Param("uid") String uid, @Param("upw") String upw) throws SQLException;
	
	public int join(UserDto userDto);

	public UserDto search(@Param("id") String id);

	public int update(UserDto userDto);

	public int delete(String uid);


	public List<String> searchUserInterest(String id);

	public List<UserDto> searchAll();

	public void addFavorite(String uid, String dong);

	public void delFavorite(String uid, String dong);

}
