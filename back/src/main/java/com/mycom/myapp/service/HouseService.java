package com.mycom.myapp.service;

import java.util.List;

import com.mycom.myapp.dto.CodeDto;
import com.mycom.myapp.dto.HouseDeal;
import com.mycom.myapp.dto.HouseInfo;

public interface HouseService {

	public List<CodeDto> sido();

	public List<CodeDto> gugun(String sidoCode);

	public List<CodeDto> dong(String gugunCode);

	public List<HouseInfo> getGeoList(String dong);
	
	public List<HouseDeal> getTransactionList(String dong, String[] checked);
	
}
