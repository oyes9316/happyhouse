package com.mycom.myapp.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycom.myapp.dao.UserDao;
import com.mycom.myapp.dto.UserDto;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Override
	public UserDto login(String uid, String upw) throws SQLException {

		return userDao.login(uid, upw);
	
	}

	@Override
	public int join(UserDto userDto) {
		
		return userDao.join(userDto);
		
	}

	@Override
	public void update(UserDto userDto) {
		
		userDao.update(userDto);

	}

	@Override
	public void delete(String uid) {
		
		userDao.delete(uid);
		
	}

	@Override
	public List<String> searchUserInterest(String id) {
		return userDao.searchUserInterest(id);
	}
	public UserDto search(String uid) {
		
		return userDao.search(uid);
		
	}

	@Override
	public List<UserDto> searchAll() {
		
		return userDao.searchAll();
		
	}

	@Override
	public void addFavorite(String uid, String dong) {
		userDao.addFavorite(uid, dong);
	}

	@Override
	public void delFavorite(String uid, String dong) {
		userDao.delFavorite(uid, dong);
	}
	
}
