package com.mycom.myapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mycom.myapp.dao.HouseDao;
import com.mycom.myapp.dto.CodeDto;
import com.mycom.myapp.dto.HouseDeal;
import com.mycom.myapp.dto.HouseInfo;

@Service
public class HouseServiceImpl implements HouseService {

	@Autowired
	HouseDao dao;

	@Override
	public List<CodeDto> sido() {
		return dao.sido();
	}

	@Override
	public List<CodeDto> gugun(String sidoCode) {
		return dao.gugun(sidoCode);
	}

	@Override
	public List<CodeDto> dong(String gugunCode) {
		return dao.dong(gugunCode);
	}

	@Override
	public List<HouseInfo> getGeoList(String dong) {
		return dao.getGeoList(dong);
	}
	@Override
	public List<HouseDeal> getTransactionList(String dong, String[] checked) {
		System.out.println("serviceImpl 진입");
		if(checked.length == 0) {
			System.out.println("null 리턴");
			return null;
		}
		System.out.println("dao 호출");
		return dao.getTransactionList(dong, checked);
	}

}
