package com.mycom.myapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mycom.myapp.dao.NoticeDao;
import com.mycom.myapp.dto.NoticeDto;

@Service
public class NoticeServiceImpl implements NoticeService {

	@Autowired
	private NoticeDao noticeDao;

	@Override
	public List<NoticeDto> retrieveNotice() {
		return noticeDao.noticeList();
	}

	@Override
	public NoticeDto detailNotice(int notice_Id) {
		System.out.println("service");
		return noticeDao.noticeDetail(notice_Id);
	}

	@Override
	public boolean writeNotice(NoticeDto noticeDto) {
		return noticeDao.noticeInsert(noticeDto) == 1;
	}

	@Override
	@Transactional
	public boolean updateNotice(NoticeDto noticeDto) {
		return noticeDao.noticeUpdate(noticeDto) == 1;
	}

	@Override
	@Transactional
	public boolean deleteNotice(int notice_Id) {
		return noticeDao.noticeDelete(notice_Id) == 1;
	}
}