package com.mycom.myapp.service;

import java.sql.SQLException;
import java.util.List;

import com.mycom.myapp.dto.UserDto;

public interface UserService {
	
	public UserDto login(String uid, String upw) throws SQLException;
	
	public int join(UserDto userDto);

	public void update(UserDto userDto);

	public void delete(String uid);

	public List<String> searchUserInterest(String id);

	public UserDto search(String uid);

	public List<UserDto> searchAll();
	
	public void addFavorite(String uid, String dong);

	public void delFavorite(String uid, String dong);
}	
