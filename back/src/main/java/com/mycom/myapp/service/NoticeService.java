package com.mycom.myapp.service;

import java.util.List;

import com.mycom.myapp.dto.NoticeDto;

public interface NoticeService {
	public List<NoticeDto> retrieveNotice();
	public NoticeDto detailNotice(int notice_Id);
	public boolean writeNotice(NoticeDto noticeDto);
	public boolean updateNotice(NoticeDto noticeDto);
	public boolean deleteNotice(int notice_Id);
}
