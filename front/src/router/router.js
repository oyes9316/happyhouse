import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Notice from "../views/Notice.vue";
import Service from "../views/Service.vue";
import Login from "../views/Login.vue";
import QnA from "../views/QnA.vue";
import Create from '../page/create.vue';
import Read from '../page/read.vue';
import Update from '../page/update.vue';
import Delete from '../page/delete.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/About",
    name: "About",
    component: About
  },
  {
    path: "/Notice",
    name: "Notice",
    component: Notice
  },
  {
    path: "/Service",
    name: "Service",
    component: Service
  },
  {
    path: "/Login",
    name: "Login",
    component: Login
  },
  {
    path: "/QnA",
    name: "QnA",
    component: QnA
  },
  {
    path: '/create',
    name: 'create',
    component: Create,
  },
  {
    path: '/read',
    name: 'read',
    component: Read,
  },
  {
    path: '/update',
    name: 'update',
    component: Update,
  },
  {
    path: '/delete',
    name: 'delete',
    component: Delete,
  }
];

const router = new VueRouter({
  routes
});

export default router;
