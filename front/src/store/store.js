import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from 'vuex-persistedstate';
import VueAlertify from 'vue-alertify';
Vue.use(VueAlertify);
Vue.use(Vuex);

import http from "@/util/http-common";
import router from "@/router/router.js";
export default new Vuex.Store({
  plugins: [
    createPersistedState()
  ],
  state: {
    isLogin: false,
    userInfo: {},
    geoList: [],
    transactionList: [],
    items: [],
    item: {}
  },
  getters: {
    items(state) {
      return state.items;
    },
    item(state) {
      return state.item;
    },
  },
  mutations: {
    mutateSetItems(state, items) {
      state.items = items
    },
    mutateSetItem(state, item) {
      state.item = item
    },

    mutateIsLogin(state, isLogin) {
      state.isLogin = isLogin;
    },
    mutateUserInfo(state, userInfo) {
      state.userInfo = userInfo
    },
    mutateSetGeoList(state, geoList) {
      state.geoList = geoList
    },
    mutateSetTransactionList(state, transactionList) {
      state.transactionList = transactionList
    },
  },
  actions: {
    getItems(context) {
      http
        .get('/notice')
        .then(({ data }) => {
          context.commit('mutateSetItems', data);
          console.log(data);
        })
        .catch(() => {
          alert('에러가 발생했습니다.');
        });
    },
    getItem(context, payload) {
      http.get(payload).then(({ data }) => {
        context.commit('mutateSetItem', data);
      });
    },
    login(context, { id, password, url }) {
      http
        .post("/login", {
          id: id,
          password: password
        })
        .then(({ data }) => {
          console.log(data);
          context.commit('mutateIsLogin', true);
          context.commit('mutateUserInfo', data);
          router.push(url);

          // 오류 코드 this in promise-then()
          // Uncaught (in promise) TypeError
          //this.$router.push('/About')
        })
        .catch((error) => {
          if (error.response.status == '404') {
            alert('아이디/비밀번호를 확인해주세요.');
          } else {
            alert('로그인 처리시 에러가 발생했습니다.');
          }
          console.log(error.config);

        });
    },
    logout(context, { url }) {
      http
        .post("/logout")
        .then((data) => {
          console.log(data);
          context.commit('mutateIsLogin', false);
          context.commit('mutateUserInfo', {});
          router.push(url);
        })
        .catch((error) => {
          alert('로그아웃 처리시 에러가 발생했습니다.');
          console.log(error.config);
        });
    },
    join(context, { id, password, name, address, phone, question, answer, url }) {
      http
        .post("join", {
          id: id,
          password: password,
          name: name,
          address: address,
          phone: phone,
          question: question,
          answer: answer
        })
        .then((data) => {
          console.log(data);
          router.push(url);
          
          alert("회원가입에 성공하였습니다.");
        })
        .catch((error) => {
          if (error.response.status == '404') {
            alert("데이터가 중복되어 실패하였습니다.");
          } else {
            alert('회원가입에 실패하였습니다.');
          }
          console.log(error.config);

        });
    },
    update(context, { id, password, name, address, phone, question, answer }) {
      http
        .post("update", {
          id: id,
          password: password,
          name: name,
          address: address,
          phone: phone,
          question: question,
          answer: answer
        })
        .then((data) => {
          console.log(data);
          context.commit('mutateUserInfo', data.data);
          alert("회원정보 수정에 성공하였습니다.");
        })
        .catch((error) => {
          alert("회원정보 수정에 실패하였습니다.");
          console.log(error.config);

        });
    },
    delete(context, { id, url }) {
      http
        .post("delete", {
          id: id,
          url: url
        })
        .then((data) => {
          console.log(data);
          console.log("들어옴");
          context.commit('mutateIsLogin', false);
          context.commit('mutateUserInfo', {});
          alert("회원정보 삭제에 성공하였습니다.");
          router.push(url);
        })
        .catch((error) => {
          alert("회원정보 삭제에 실패하였습니다.");
          console.log(error.config);

        });
    }
  },
  setGeoList(context, geoList) {
    context.commit('mutateSetGeoList', geoList)
  },
  setTransactionList(context, transactionList) {
    context.commit('mutateSetTransactionList', transactionList)
  },
  // getters: {
  //   getGeoList: function (state) {
  //     return this.$state.geoList;
  //   },
  //   getTransactionList: function (state) {
  //     return state.transactionList;
  //   }
  // },
  modules: {}
});
